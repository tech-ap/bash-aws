# Bash Toolset for simple access of instances in AWS-Spot setup with changing IP Adresses

Contains some bash *functions* to find out the dynamic IP adresses of spot instances

## function aws_public_ips_of

Provides a list of IP adresses of instances tagged with type=***
Params:
 $1 AWS profile
 $2 type of instance (e.g. web)

## function aws_public_ip_of_instance

Provides a SINGLE IP address of an instance tagged with Name=***
 $1 AWS profile
 $2 Filter for Instance tag Name

## function sshawsinstance

Connects an instance vi SSH using 3 parameters
 $1 AWS profile
 $2 User name
 $3 Filter for Instance tag Name

