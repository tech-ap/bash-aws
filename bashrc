# Lists public IP's of instances in AWS tagged with type=X
# Params: AWS profile, type of instance
function aws_public_ips_of {
        aws ec2 describe-instances --profile $1 --filters "Name=tag:type,Values=$2" --query "Reservations[*].Instances[*].PublicIpAddress" --output text;
}

function sshawsinstance {
        ssh $2@$(aws_public_ip_of_instance $3 $1);
}

function aws_public_ip_of_instance {
        aws ec2 describe-instances --profile $1 --filters "Name=tag:Name,Values=$2" --query "Reservations[*].Instances[*].PublicIpAddress" --output text;
}

